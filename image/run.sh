#!/bin/bash

####

# the main shell script (entrypoint in docker terminology)
# which must use these 3 locations inside docker image:

# 1, directory containing all input data sets: /input , which includes a subdirectory /input/run_test containing the test data sets to feed run.sh and obtain the task comparables (not applicable to reproduction task E.1).
# 2, directory for output data sets: /output/datasets
# 3, directory for output comparables, including scores, tables and/or plots, etc. (i.e. the major reproduction comparables of the respective task, indicated in the call for papers): /output/tables_and_plots

####


# note prescribed directory structure
#$/ tree
#.
#├── input                   #directory containing all input data sets (actual data will be provided at runtime)
#└── output                  #directory containing output ouput data
#    ├── datasets            #directory for output data sets
#    └── tables_and_plots    #directory for output comparables, including scores, tables and/or plots,
#                            #etc. (i.e. the respective major reproduction comparables indicated in the call for papers)
#
####


# declare paths
MERLIN=input/meta_ltext/
TEXTS=input/text_only/
mkdir -p $MERLIN
mkdir -p $TEXTS
OUT=output/datasets/
LOGS=$OUT/logs/
mkdir -p $LOGS
TABLES=output/tables_and_plots/
LANGTOOL=langtool/
UDPIPE=udpipe/


# REPROLANG 2020
ls -l

# and run corpus prep with io argvars
python 01_corpusCollation.py $MERLIN $TEXTS

# feature extraction
Rscript 02_featureExtraction.R $UDPIPE $LANGTOOL $TEXTS $OUT

# experiments
Rscript 03_classificationExperiments_parallel_crossling.R $OUT
Rscript 03_classificationExperiments_parallel_monoling.R $OUT
Rscript 03_classificationExperiments_parallel_multiling.R $OUT

# print results
Rscript 04_resultsSummary.R $LOGS $TABLES
